Description: allow tests to run on sound cards with no PCM
Author: Paul Brossier <piem@debian.org>
Last-Update: 2018-09-21

--- a/test.py
+++ b/test.py
@@ -41,6 +41,12 @@
 class MixerTest(unittest.TestCase):
     """Test Mixer objects"""
 
+    def tryOpenMixer(self):
+        try:
+            return alsaaudio.mixers()
+        except alsaaudio.ALSAAudioError as e:
+            self.skipTest(repr(e))
+
     def testMixer(self):
         """Open the default Mixers and the Mixers on every card"""
         
@@ -54,7 +60,7 @@
     def testMixerAll(self):
         "Run common Mixer methods on an open object"
 
-        mixers = alsaaudio.mixers()
+        mixers = self.tryOpenMixer() #alsaaudio.mixers()
         mixer = alsaaudio.Mixer(mixers[0])
 
         for m, a in MixerMethods:
@@ -70,7 +76,7 @@
         """Run common Mixer methods on a closed object and verify it raises an 
         error"""
 
-        mixers = alsaaudio.mixers()
+        mixers = self.tryOpenMixer() #alsaaudio.mixers()
         mixer = alsaaudio.Mixer(mixers[0])
         mixer.close()
 
@@ -84,17 +90,26 @@
 class PCMTest(unittest.TestCase):
     """Test PCM objects"""
 
+    def tryOpenPCM(self):
+        try:
+            return alsaaudio.PCM()
+        except alsaaudio.ALSAAudioError as e:
+            self.skipTest(repr(e))
+
     def testPCM(self):
         "Open a PCM object on every card"
 
         for c in alsaaudio.card_indexes():
-            pcm = alsaaudio.PCM(cardindex=c)
-            pcm.close()
+            try:
+                pcm = alsaaudio.PCM(cardindex=c)
+                pcm.close()
+            except alsaaudio.ALSAAudioError as e:
+                self.skipTest(repr(e))
 
     def testPCMAll(self):
         "Run all PCM methods on an open object"
 
-        pcm = alsaaudio.PCM()
+        pcm = self.tryOpenPCM() #alsaaudio.PCM()
 
         for m, a in PCMMethods:
             f = alsaaudio.PCM.__dict__[m]
@@ -108,7 +123,7 @@
     def testPCMClose(self):
         "Run all PCM methods on a closed object and verify it raises an error"
 
-        pcm = alsaaudio.PCM()
+        pcm = self.tryOpenPCM() #alsaaudio.PCM()
         pcm.close()
 
         for m, a in PCMMethods:
