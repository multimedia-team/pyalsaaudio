Description: use // to avoid conversion to float in py3
Author: Paul Brossier <piem@debian.org>
Last-Update: 2018-09-21

--- pyalsaaudio-0.8.4.orig/playwav.py
+++ pyalsaaudio-0.8.4/playwav.py
@@ -30,7 +30,7 @@ def play(device, f):
     else:
         raise ValueError('Unsupported format')
 
-    periodsize = f.getframerate() / 8
+    periodsize = f.getframerate() // 8
 
     device.setperiodsize(periodsize)
     
